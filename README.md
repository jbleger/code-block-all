# Code-block-all Extension For Quarto

This extension provide decoration on code blocks **and** code outputs (display,
stdout, and stderr). See output of examples at the end of this `README`.

## Installing

```bash
quarto add https://gitlab.com/jbleger/code-block-all/-/archive/main/shorty-main.zip
```

This will install the extension under the `_extensions` subdirectory.
If you're using version control, you will want to check in this directory.

## Using

Two versions are provided:

- a lua filter for use in LaTeX/pdf formats
- a scss stylesheet for html output.

### LaTeX/pdf formats:

Use in you metadata:

```
filters:
  - code-block-all
```

### HTML format

You have two way to use the stylesheet:

- use the `code-block-all-html` format.
- symlink the scss:
  ```bash
    ln -s _extensions/code-block-all/code-block-all.scss .
  ```

  and use the scss in your html (or derivative) format by adding in your
  metadata:
  ```
  format:
    html:
      theme: code-block-all.scss
  ```

  or if you have many:
  ```
  format:
    html:
      theme: [code-block-all.scss, …]
  ```

## Example

Examples are given for Python and R.

 - python example:
   [source file][pyqmd], [pdf output][pypdf], [html output][pyhtml]
 - R example:
   [source file][rqmd], [pdf output][rpdf], [html output][rhtml]

[pyqmd]: sample_python.qmd
[rqmd]: sample_r.qmd
[pypdf]: https://jbleger.gitlab.io/code-block-all/sample_python.pdf
[pyhtml]: https://jbleger.gitlab.io/code-block-all/sample_python.html
[rpdf]: https://jbleger.gitlab.io/code-block-all/sample_r.pdf
[rhtml]: https://jbleger.gitlab.io/code-block-all/sample_r.html
